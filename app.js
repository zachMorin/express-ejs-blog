var bodyParser = require('body-parser'),
mongoose       = require('mongoose'),
express        = require("express"),
app            = express();

//APP CONFIG
mongoose.connect("mongodb://localhost/restful_blog_app");
app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(bodyParser.urlencoded({extended: true}));

//MONGOOSE MODEL CONFIG
var blogSchema = new mongoose.Schema({
    title: String,
    image: String,
    body: String,
    created: {type: Date, default: Date.now}
});
// make us a model with the schema and has different methods available
// "Blog" creates a collection in db and db will make it plural, 1st param is the collection 2nd is the schema
var Blog = mongoose.model("Blog", blogSchema);

//RESFTUL ROUTES
app.get("/", function(req,res){
    res.redirect("/blogs");
});

//INDEX ROUTE
app.get("/blogs", function(req,res){
    Blog.find({}, function(err,blogs){
        if(err){
            console.log(err);
        } else {
            res.render("index", {blogs: blogs});
        }
    });
});

//NEW ROUTE
app.get("/blogs/new", function(req,res){
    res.render("new");
});

//CREATE ROUTE
app.post("/blogs", function (req,res){
    var data = req.body.blog;
    //create blog
    Blog.create(data, function(err,newBlog){
        if(err){
            res.render("new")
        } else {
            //then, redirect to the index
            res.redirect("/blogs");
        }
    });
});

app.listen(3000, function(){
    console.log("Server has started");
});